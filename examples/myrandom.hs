#!/usr/bin/env nix-shell
#!nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.random])"

{-# LANGUAGE ScopedTypeVariables #-}
import System.Random
main = do
    x::Double <- randomIO
    print x

