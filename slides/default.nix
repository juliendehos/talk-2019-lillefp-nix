with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "lillefp-2019-nix";
  src = ./.;
  buildInputs = [
    gnumake
    librsvg
    pandoc
    #texlive.combined.scheme-small
  ];
  installPhase = ''
    mkdir -p $out
    cp -R files index.html $out
  '';
}

