---
title: An overview of <br> the Nix ecosystem
subtitle: <table> <tr> <td><img style="border:0" src="files/lillefp.png" height="150px"/></td> <td style="vertical-align:middle"> LilleFP14<br>2019-04-25 </td> </tr> </table>
---

## by Julien Dehos

- assistant professor
- Université du Littoral Côte d'Opale
- Nix user since NixOS-16.03
- <http://juliendehos.gitlab.io>





# The Nix project <br> ![](files/nixos-logo-small.png){width="200px"}

## History

- Eelco Dolstra, *The Purely Functional Software Deployment Model*, PhD thesis, Utrecht, 2006
- github projects: <https://github.com/NixOS> <br> ![](files/nixos-github.png)
- GNU Guix: <https://www.gnu.org/software/guix>

## Motivation

- purely functional packaging system
- reproducible/isolated/composable packages
- reliable upgrades
- declarative configuration model
- ...

## Functional packaging

![](files/guix_fp.png)

## Implementation

- no Filesystem Hierarchy Standard (FHS)
- `/nix/store/`
- hash codes
- symbolic links + profiles/environments

--------------------------------------------------

![](files/user-environments.png)

## Dependency graph

![](files/python-deps.dot.png){width="500px"}

## Derivation

`examples/hello.nix`:

```nix
with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "hello-2.10";
  src = fetchurl {
    url = "mirror://gnu/hello/hello-2.10.tar.gz";
    sha256 = "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i";
  };
}
```

## Build a derivation

```text
$ nix-build hello.nix

these derivations will be built:
  /nix/store/iclkh6vn27jga8xvwpyv3qx1crjcrw8i-hello-2.10.drv
...
/nix/store/kl7sr2m0cds7ag8b23g2i58cn2b0xyqj-hello-2.10
```

$+$ a symbolic link `./result`

## Result

```text
$ tree ./result

    result
    |-- bin
    |   |-- hello
    ...

$ ./result/bin/hello

    Bonjour, le monde !
```

## The Nix ecosystem

- Nix: language
- Nix: tools (nix-env, nix-build, nix-shell...)
- Nixpkgs: package collection
- NixOS: linux distribution
- Nixops: deployment tool
- Hydra, disnix...

## The Nix language

- pure, lazy, functional language
- [Nix manual, Chapter 15](https://nixos.org/nix/manual/#ch-expression-language)
- [A tour of Nix](https://nixcloud.io/tour)
- [Gaël Deest, *Nix : Déploiement purement fonctionnel*, Lambda Rennes, 2019](https://www.youtube.com/watch?v=DpUyB7KTePo)





# Computer configuration <br> ![](files/spongebob-computer.jpg){width="400px"}

## Nix can configure a system

- configs, packages, services
- declarative configuration model
- NixOS
- Nix on Linux/macOS


## Installing Nix on Linux/macOS

```text
sh <(curl https://nixos.org/nix/install) --daemon
```

[Nix manual, Chapter 4](https://nixos.org/nix/manual/#ch-installing-binary)


## Installing NixOS

```text
cfdisk /dev/sda  # ...
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
nixos-generate-config --root /mnt
vi /mnt/etc/nixos/configuration.nix  # ...
nixos-install
```

[NixOS manual, Chapter 2](https://nixos.org/nixos/manual/index.html#sec-installation)



## System configuration

- NixOS
- superuser
- `/etc/nixos/configuration.nix`

--------------------------------------------------

```nix
{ config, pkgs, ... }:
{
  networking.hostName = "my-nixos";      # configure options
  time.timeZone = "Europe/Paris";

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  virtualisation.docker.enable = true;
  virtualisation.virtualbox.host.enable = true;
```

--------------------------------------------------

```nix
  environment.systemPackages = with pkgs; [  # add packages
    file git htop tmux tree vim wget
  ];

  services.postgresql = {             # activate a service
    enable = true;
    authentication = ''
      local all all trust
    '';
  };

  # ...
}
```

--------------------------------------------------

update the system:

```text
# nixos-rebuild switch
```

$+$ rollback, generations...


## User configuration

- NixOS or Nix
- user
- imperative management: `nix-env -i/-e`
- or "packageOverrides"
- or "overlays": fixed-point layer composition
- or "home manager": declarative configuration

## User configuration with packageOverrides

`~/.config/nixpkgs/config.nix`:

```nix
with import <nixos> {};
{
  packageOverrides = pkgs: with pkgs; {
    myPackages = buildEnv {       # create a package...
      name = "myPackages";
      paths = [
        firefox          # ... including nixos packages
        libreoffice
      ];
    };
  };
}
```

--------------------------------------------------

install/update:

```text
$ nix-env -iA nixos.myPackages
```

--------------------------------------------------

## Configuring Vim with Nix

`~/.config/nixpkgs/vim.nix`:

```nix
{ pkgs }:

pkgs.vim_configurable.customize {
  name = "vim";
  # add Vim plugins (managed by Nix)
  vimrcConfig.packages.myVimPackage = with pkgs.vimPlugins; {
    start = [ ale easymotion ];
  };
  # my vim config 
  vimrcConfig.customRC = ''
    let g:ale_linters = {'haskell': ['hlint', 'ghc']}
  '';
}
```

--------------------------------------------------

`~/.config/nixpkgs/config.nix`:

```nix
with import <nixos> {};
{
  packageOverrides = pkgs: with pkgs; {
    myPackages = buildEnv {
      name = "myPackages";
      paths = [
        firefox
        libreoffice
        (callPackage ./vim.nix {})  # include the vim config
      ];
    };
  };
}
```

--------------------------------------------------

list available vim plugins:

```text
$ nix-env -qaP -A nixos.vimPlugins

vimPlugins.pluginnames2nix    vim-plugin-names-to-nix
vimPlugins.a-vim              vimplugin-a-vim-2010-11-06
vimPlugins.ack-vim            vimplugin-ack-vim-2018-02-28
vimPlugins.acp                vimplugin-acp-2013-02-05
vimPlugins.agda-vim           vimplugin-agda-vim-2018-05-23
vimPlugins.ale                vimplugin-ale-2018-07-25
...
```






# Software packaging <br> ![](files/spongebob-package.jpg){width="400px"}


## Nix can package software

- in C/C++, Python, Haskell...
- by writing Nix derivations (`default.nix`) 
- and using [Nixpkgs](https://nixos.org/nixpkgs/manual)

## Example: C++/cmake project

`showimg/CMakeLists.txt`:

```cmake
cmake_minimum_required( VERSION 2.8 )
project( showimg )

find_package( PkgConfig REQUIRED )
pkg_check_modules( MYPKG REQUIRED opencv )
include_directories( ${MYPKG_INCLUDE_DIRS} )

add_executable( showimg showimg.cpp )
target_link_libraries( showimg ${MYPKG_LIBRARIES} )
install( TARGETS showimg DESTINATION bin )
```

dependencies: opencv, pkg-config, cmake


## Example: classic workflow

```console
sudo apt-get install cmake pkg-config libopencv-dev
mkdir build
cd build
cmake ..
make
sudo make install
```

## Example: Nix packaging

```nix
with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "showimg";
  src = ./.;
  buildInputs = [ cmake pkgconfig opencv ];
}
```

--------------------------------------------------

build derivation:

```text
$ nix-build

    these derivations will be built:
      /nix/store/axzyh26z6k60yi7nsz71i4fk6734lgdl-showimg.drv
    ...
    /nix/store/2lpqmmyvaz83awzpxxzwqansj8rhkz0v-showimg

$ ./result/bin/showimg bob.png 
```

Nix knows how to build a cmake project !

--------------------------------------------------

install/remove derivation:

```text
$ nix-env -i -f .

$ showimg bob.png 

$ nix-env -e showimg
```

--------------------------------------------------

run environment:

```text
$ nix-shell 

[nix-shell]$ mkdir mybuild

[nix-shell]$ cd mybuild/

[nix-shell]$ cmake ..
```

## Example: Nix packaging (2)

Nix can override many options:

```nix
# pin package channel
{ pkgs ? import (fetchTarball 
    "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {}
}: with pkgs;

let

  # override GCC version
  _stdenv = overrideCC stdenv gcc6;
```

--------------------------------------------------

```nix
  # override package parameters
  _opencv_1 = opencv.override {
    stdenv = _stdenv;
    cmake = cmake_2_8; 
    enableGtk2 = true; 
  };

  # override package internal attributes
  _opencv_2 = _opencv_1.overrideDerivation (attrs: {
    cmakeFlags = [ 
      attrs.cmakeFlags 
      "-DENABLE_PRECOMPILED_HEADERS=OFF" ];
  });
```

see the [opencv derivation](https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/opencv/default.nix)

--------------------------------------------------

```nix
in

_stdenv.mkDerivation {
  name = "showimg";
  src = ./.;
  buildInputs = [ cmake pkgconfig _opencv_2 ];
}
```

```text
$ nix-shell
...

[nix-shell] $ gcc --version
gcc (GCC) 6.4.0
```





# Development environment <br> ![](files/spongebob-env.jpg){width="350px"}

## Nix environments

- useful for setting up a project
- ad-hoc environments, `shell.nix`, `default.nix`
- [Nixpkgs manual](https://nixos.org/nixpkgs/manual)

## Example 1: GNU hello

ad-hoc environment:

```text
$ nix-shell -p hello

[nix-shell]$ hello
Bonjour, le monde !

[nix-shell]$ exit
```

```text
$ nix-shell -p hello --run hello
Bonjour, le monde !
```

--------------------------------------------------

`examples/hello/shell.nix`:

```nix
with import <nixpkgs> {};

mkShell {
  buildInputs = [
    hello
  ];
}
```

```text
$ nix-shell --run hello
Bonjour, le monde !
```

## Example 2: Python packages

python virtual environment:

```text
$ virtualenv -p python3 myenv
...

$ source myenv/bin/activate

(myenv) $ pip install numpy
...

(myenv) $ python
Python 3.6.8 
>>> import numpy
>>> 
```

--------------------------------------------------

Nix ad-hoc environment:

```text
$ nix-shell -p 'python3.withPackages(ps: [ps.numpy])'

[nix-shell]$ python
Python 3.6.8  
>>> import numpy
>>> 
```

--------------------------------------------------

`examples/numpy/shell.nix`:

```nix
with import <nixpkgs> {};

(python3.withPackages ( ps: with ps; [
  numpy
])).env
```

```text
$ nix-shell --run python
Python 3.6.8  
>>> import numpy
>>> 
```

## Example 3: composing environments

```nix
with import <nixpkgs> {};
let
 showimg = pkgs.callPackage ../showimg/default.nix {};
in
mkShell {
  buildInputs = [
    showimg
    python3
  ];
}
```

```text
$ nix-shell --run python
Python 3.6.8
>>> import os
>>> os.system('showimg bob.png')
```






# Haskell <br> ![](files/spongebob-haskell.png){width="250px"}

## Haskell infrastructure in Nix

- many Haskell packages in Nixpkgs
- toolchains: Nix, Nix+Cabal, Nix+Stack
- [Nixpkgs manual, 9.5](https://nixos.org/nixpkgs/manual/#users-guide-to-the-haskell-infrastructure)
- [Nix and Haskell in production](https://github.com/Gabriel439/haskell-nix)


## List Haskell packages/compilers

```text
$ nix-env -qaP -A nixos.haskellPackages
nixos.haskellPackages.a50                 a50-0.5
nixos.haskellPackages.AAI                 AAI-0.2.0.1
nixos.haskellPackages.abacate             abacate-0.0.0.0
...
```

```text
$ nix-env -qaP -A nixos.haskell.compiler
nixos.haskell.compiler.ghc7103            ghc-7.10.3
nixos.haskell.compiler.ghc802             ghc-8.0.2
nixos.haskell.compiler.ghc822             ghc-8.2.2
...
```


## Ad-hoc environments

```text
$ nix-shell --run ghci -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.random])"

GHCi, version 8.4.4: http://www.haskell.org/ghc/  :? for help
Prelude> import System.Random 
Prelude System.Random> :set -XScopedTypeVariables 
Prelude System.Random> x::Double <- randomIO 
Prelude System.Random> x
0.114730187676368
```

## Script files

```haskell
#!/usr/bin/env nix-shell
#!nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.random])"

{-# LANGUAGE ScopedTypeVariables #-}
import System.Random
main = do
    x::Double <- randomIO
    print x
```

```text
$ ./myrandom.hs 
0.9742300417428591
```

## Cabal (example 1)

`myrandom/myrandom.cabal`:

```cabal
name:                myrandom
version:             0.1
build-type:          Simple
cabal-version:       >=1.10
license:             MIT

executable myrandom
  main-is:             myrandom.hs
  default-language:    Haskell2010
  build-depends:       base, random
```

--------------------------------------------------

`myrandom/default.nix`:

```nix
{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "myrandom" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
```

--------------------------------------------------

run environment:

```text
$ nix-shell 
...

[nix-shell]$ cabal run
...
Running myrandom...
0.29070504359756477
```

--------------------------------------------------

build derivation:

```text
$ nix-build 
...

$ ./result/bin/myrandom 
0.6441685176316507
```

--------------------------------------------------

install derivation:

```text
$ nix-env -i -f .

$ myrandom 
0.9953143854386685
```

## Cabal (example 2)

`myrandom/default2.nix`:

```nix
let
  config = { 
    packageOverrides = pkgs: {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {
          ghc = pkgs.haskell.packages.ghc822;
  };};};};

  pkgs = import <nixpkgs> { inherit config; };

  drv = pkgs.haskell.packages.ghc.callCabal2nix "myrandom" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
```

--------------------------------------------------

```text
$ nix-shell default2.nix

[nix-shell]$ ghc --version
The Glorious Glasgow Haskell Compilation System, version 8.2.2
```


## Stack

`stack.yaml`:

```yaml
resolver: lts-9.21
```

```text
$ stack --nix build --exec myrandom
...
0.8784417204074236
```

[Stack, Nix integration](https://docs.haskellstack.org/en/stable/nix_integration/)




# Continuous Integration <br> ![](files/spongebob-ci.jpg){width="400px"}

## Nix for CI 

- Nix derivations in CI pipeline 
- Nix CI service: Hydra 
- [Hydra NixOS wiki](https://nixos.wiki/wiki/Hydra)
- [Hydra manual](https://nixos.org/hydra/manual)

## Quick & dirty gitlab-ci

`myrandom/.gitlab-ci.yml`:

```yaml
test:
  image: nixos/nix
  before_script:
    - nix-env -iA nixpkgs.cabal-install
  script:
    - nix-shell --run "cabal --http-transport=plain-http test"
```

--------------------------------------------------

![](files/gitlab-ci.png)


## Hydra: installation on NixOS

`/etc/nixos/configuration.nix`:

```nix
services.hydra = {
    enable = true;
    hydraURL = "http://localhost:3000";
    notificationSender = "hydra@localhost";
    buildMachinesFiles = [];
    useSubstitutes = true;
};
```

```text
# su - hydra

[hydra@nixos:~]$ hydra-create-user toto --password toto --role admin
creating new user `toto'
```

## Hydra: CI pipeline

`myrandom/release1.nix`:

```nix
let
  pkgs = import <nixpkgs> { };
in
  {
    myrandom = pkgs.haskellPackages.callPackage ./default.nix { };
  }
```

--------------------------------------------------

![](files/hydra-4.png)

--------------------------------------------------

![](files/hydra-5.png)

## Hydra: other jobs

- multiple systems/platforms 
- source/binary tarballs
- test coverage
- ...



# Deployment <br> ![](files/spongebob-deployment.jpg){width="400px"}

## Nix deployment tools

- dockerTools: docker images
- nixops: NixOS machines
- nixos-container, cachix, kubenix...

## Docker images

- Nix can define and build Docker images
- [Nixpkgs manual, 7.7](https://nixos.org/nixpkgs/manual/#sec-pkgs-dockerTools)
- [Gaël Deest, *Nix : Déploiement purement fonctionnel*, Lambda Rennes, 2019](https://www.youtube.com/watch?v=DpUyB7KTePo)

--------------------------------------------------

`myrandom/myrandom-docker.nix`:

```nix
{ pkgs ? import <nixpkgs> {} }:
let
  _myrandom = pkgs.haskellPackages.callCabal2nix "myrandom" ./. {};
  _app = pkgs.haskell.lib.justStaticExecutables _myrandom;
in
pkgs.dockerTools.buildLayeredImage {
  name = "myrandom";
  tag = "latest";
  config.Cmd = [ "${_app}/bin/myrandom" ];
}
```

--------------------------------------------------

```text
$ nix-build myrandom-docker.nix 
building '/nix/store/0rmrz4fvhdh6vjyrbqnwfrxgnfp4x9sw-cabal2nix-myrandom.drv'...
...

$ docker load < result 
1114fa18ba6b: Loading layer [========>]  10.24kB/10.24kB
...
```

--------------------------------------------------

```nix
$ docker images
REPOSITORY   TAG      IMAGE ID        CREATED        SIZE
myrandom     latest   14609c13b4e6    49 years ago   32MB

$ docker run --rm -it myrandom:latest
0.6054645500126655
```


## Nixops

- Nixops can deploy NixOS machines
- AWS, GCE, Azure, VirtualBox...
- declarative configuration model
- [Nixops manual](https://nixos.org/nixops/manual/)


## Nixops: example 

`myusers`:

- PostgreSQL database 
- HTTP server (Haskell)
- VirtualBox VM

--------------------------------------------------

files:

- default.nix
- myusers.cabal
- myusers.hs
- myusers.sql
- myusers-nixops.nix


--------------------------------------------------

`myusers-nixops.nix`:

```nix
{
  network.description = "myusers";

  webserver = { config, pkgs, ... }: 

  let
    _myusers = pkgs.callPackage ./default.nix {};
  in
  {
    networking.firewall.allowedTCPPorts = [ 8080 ];
```

--------------------------------------------------

```nix
    services.postgresql = {
      enable = true;
      initialScript = "${_myusers}/myusers.sql";
      authentication = "local all all trust";
    };

    systemd.services.myusers = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "postgresql.service" ];
      script = "${_myusers}/myusers";
    };
```

--------------------------------------------------

```nix
    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}
```



--------------------------------------------------

deployment:

```text
$ nixops create -d vm1 myusers-nixops.nix
...

$ nixops deploy -d vm1 --force-reboot
...
```

--------------------------------------------------

![](files/nixops.png)

--------------------------------------------------

```text
$ nixops ssh -d vm1 webserver

[root@webserver:~]# systemctl status myusers.service 
. myusers.service
...
```



# Conclusion

## Nix ecosystem

- functional packaging system, functional language
- configs, packages, environments, CI, deployment...
- reproducible, isolated, composable...
- very promising approach


## But

- very different approach
- learning curve
- needs a lot of disk space



## References

- these slides: <br> <https://juliendehos.gitlab.io/lillefp-2019-nix>
- source code: <br> <https://gitlab.com/juliendehos/lillefp-2019-nix>
- <https://nixos.org>
- <https://nixos.wiki>

--------------------------------------------------

Thank you !

Questions & comments ?

--------------------------------------------------


