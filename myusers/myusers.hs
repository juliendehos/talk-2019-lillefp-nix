{-# LANGUAGE OverloadedStrings #-}
import           Control.Monad.Trans (liftIO)
import           Control.Monad (forM_)
import qualified Data.Text.Lazy as L
import qualified Database.PostgreSQL.Simple as SQL
import           Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import           Lucid
import           Web.Scotty (html, scotty, html, get)

data User = User { firstname :: L.Text, lastname :: L.Text } 

instance FromRow User where
    fromRow = User <$> field <*> field 

dbParams = "host=localhost port=5432 dbname=myusersdb user=toto password='toto'"

getUsers :: IO [User]
getUsers = do
    conn <- SQL.connectPostgreSQL dbParams
    users <- SQL.query_ conn "SELECT firstname, lastname FROM users"
    SQL.close conn
    return users

main = scotty 8080 $ get "/" $ do
    users <- liftIO getUsers
    html $ renderText $ html_ $ body_ $ do
        p_ "users:"
        ul_ $ forM_ users $ \(User f l) -> li_ $ toHtml $ L.concat [l, ", " , f]

