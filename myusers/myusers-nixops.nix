{
  network.description = "myusers";

  webserver = { config, pkgs, ... }: 
  let
    _myusers = pkgs.callPackage ./default.nix {};
  in
  {
    networking.firewall.allowedTCPPorts = [ 8080 ];

    systemd.services.myusers = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "postgresql.service" ];
      script = "${_myusers}/myusers";
    };

    services.postgresql = {
      enable = true;
      initialScript = "${_myusers}/myusers.sql";
      authentication = "local all all trust";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}

