{ pkgs ? import <nixpkgs> {} }:

let

  drv1 = pkgs.haskellPackages.callCabal2nix "myusers" ./. {};

  drv2 = pkgs.stdenv.mkDerivation {
    name = "myusers";
    src = ./.;
    buildInput = [
      drv1
    ];
    installPhase = ''
      mkdir -p $out
      cp ${drv1}/bin/myusers $out/
      cp myusers.sql $out/
    '';
  };

in

if pkgs.lib.inNixShell then drv1.env else drv2

