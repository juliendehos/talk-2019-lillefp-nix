CREATE DATABASE myusersdb;
\connect myusersdb

CREATE TABLE users (
  id SERIAL PRIMARY KEY, 
  firstname TEXT,
  lastname TEXT 
);
INSERT INTO users (firstname, lastname) VALUES('John', 'Doe');
INSERT INTO users (firstname, lastname) VALUES('Bob', 'Sponge');

CREATE ROLE toto WITH LOGIN PASSWORD 'toto';
GRANT SELECT ON TABLE users TO toto;

-- psql -U postgres -f myusers.sql

