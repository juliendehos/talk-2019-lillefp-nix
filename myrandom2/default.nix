let

  nixpkgs = import <nixpkgs> {};

  hpkgs = nixpkgs.haskell.packages.ghc864;

  myrandom = hpkgs.callCabal2nix "myrandom" ./. {};

  shell = (
    nixpkgs.haskell.lib.addBuildTools myrandom [
      hpkgs.cabal-install
      hpkgs.ghcid
      hpkgs.hoogle
      hpkgs.stylish-cabal
    ]
  ).env;

in if nixpkgs.lib.inNixShell then shell else myrandom

