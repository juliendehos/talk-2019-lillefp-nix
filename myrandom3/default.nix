let
  pkgs = import <nixpkgs> {};
  compilerVersion = "ghc864";
  compiler = pkgs.haskell.packages."${compilerVersion}";
in
  compiler.developPackage {
    root = ./.;

    source-overrides = import ./overrides.nix { inherit pkgs; };

    #source-overrides = {
    #  lucid = "2.9.11";
    #};

    #overrides = self: super: with pkgs.haskell.lib; {
    #  random = dontCheck super.random;
    #};

  }

