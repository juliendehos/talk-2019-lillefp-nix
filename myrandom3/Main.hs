{-# LANGUAGE OverloadedStrings #-}
import System.Random
import Lucid

myPage :: Double -> Html ()
myPage x = do
  doctype_
  html_ $ do
    head_ $ do
      title_ "myrandom"
      meta_ [charset_ "utf-8"]
    body_ $ do
      h1_ "myrandom"
      p_ $ toHtml $ show x

main :: IO ()
main = do
  x <- randomIO
  print $ renderText $ myPage x
  renderToFile "myrandom.html" $ myPage x

