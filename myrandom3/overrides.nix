{ pkgs }: {
  lucid = pkgs.fetchFromGitHub {
    owner = "chrisdone";
    repo = "lucid";
    rev = "v2.9.11";
    sha256 = "sha256:0yzndnl2fvbyd02s80liniqi9addzwc01jq85gzds6pl01z0a58j";
  };
}
