{ pkgs ? import <nixpkgs> {} }:
let
  _myrandom = pkgs.callPackage ./default.nix {};
in
pkgs.dockerTools.buildLayeredImage {
  name = "myrandom";
  tag = "latest";
  config.Cmd = [ "${_myrandom}/bin/myrandom" ];
}

