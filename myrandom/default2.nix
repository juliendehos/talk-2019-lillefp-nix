let
  config = { 
    packageOverrides = pkgs: {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {
          ghc = pkgs.haskell.packages.ghc822;
          };
        };
      };
    };
  pkgs = import <nixpkgs> { inherit config; };
  drv = pkgs.haskell.packages.ghc.callCabal2nix "myrandom" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv

