{ pkgs ? import <nixpkgs> {} }:
let
  _myrandom = pkgs.haskellPackages.callCabal2nix "myrandom" ./. {};
  _app = pkgs.haskell.lib.justStaticExecutables _myrandom;
in
pkgs.dockerTools.buildLayeredImage {
  name = "myrandom";
  tag = "latest";
  config.Cmd = [ "${_app}/bin/myrandom" ];
}

