{ config, pkgs, ... }:
{
  imports = [ ./hardware-configuration.nix ];

  networking.hostName = "my-nixos";
  system.stateVersion = "18.09";
  time.timeZone = "Europe/Paris";

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  i18n.consoleFont = "Lat2-Terminus16";
  i18n.consoleKeyMap = "fr-bepo";
  i18n.defaultLocale = "fr_FR.UTF-8";

  environment.systemPackages = with pkgs; [
    file git htop tmux tree vim wget
  ];

  virtualisation.docker.enable = true;
  virtualisation.virtualbox.host.enable = true;

  services.postgresql = {
    enable = true;
    authentication = ''
      local all all trust
    '';
  };

}

