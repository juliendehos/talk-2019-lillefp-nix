with import <nixos> {};
{
  packageOverrides = pkgs: with pkgs; {
    myPackages = buildEnv {
      name = "myPackages";
      paths = [ firefox ];
    };
    myVim = buildEnv {
      name = "myVim";
      paths = [ (callPackage ./vim.nix {}) ];
    };
  };
}

