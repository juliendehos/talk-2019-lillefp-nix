#include <opencv2/opencv.hpp>

int main(int argc, char ** argv) {
    cv::Mat img = cv::imread(argv[1], cv::IMREAD_ANYDEPTH
                                    | cv::IMREAD_ANYCOLOR);
    cv::imshow("mydisplay", img);
    cv::waitKey();
    return 0;
}

