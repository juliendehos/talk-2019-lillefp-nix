with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "showimg";
  src = ./.;
  buildInputs = [ cmake pkgconfig opencv ];
}

