# use the nixos-18.09 channel
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {}}:
with pkgs;

let

  _stdenv = overrideCC stdenv gcc6;  # use GCC6

  _opencv = (opencv.override {  # opencv parameters
    stdenv = _stdenv;
    cmake = cmake_2_8; 
    enableGtk2 = true; 
  }).overrideDerivation (attrs: {  # opencv internal parameters
    cmakeFlags = [ 
      attrs.cmakeFlags 
      "-DENABLE_PRECOMPILED_HEADERS=OFF"
    ];
  });

in

_stdenv.mkDerivation {
  name = "showimg";
  src = ./.;
  buildInputs = [ cmake pkgconfig _opencv ];
}

