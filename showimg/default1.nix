with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "showimg";
  src = ./.;
  buildInputs = [ cmake pkgconfig opencv ];
  configurePhase = ''
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=$out ..
  '';
  buildPhase = "make";
  installPhase = "make install";
}

