with import <nixpkgs> {};
let
 showimg = pkgs.callPackage ../showimg/default.nix {};
in

mkShell {
  buildInputs = [
    showimg
    python3
  ];
}

